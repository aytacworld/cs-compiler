using System;
using System.IO;
using System.Xml.Serialization;

namespace Com.Aytacworld
{
    public class ConfigGenerator
    {
        public static Configuration Create(string filePath)
        {
            var reader = new XmlSerializer(typeof(Configuration));
            var loadStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var ret = (Configuration)reader.Deserialize(loadStream);
            loadStream.Close();
            return ret;
        }
    }

    [XmlRoot("configuration"), Serializable]
    public class Configuration
    {
        [XmlElement("application")]
        public Application Application { get; set; }
    }

    public class Application
    {
        [XmlAttribute("title")]
        public string Title { get; set; }

        [XmlAttribute("description")]
        public string Description { get; set; }

        [XmlAttribute("company")]
        public string Company { get; set; }

        [XmlAttribute("product")]
        public string Product { get; set; }

        [XmlAttribute("copyright")]
        public string Copyright { get; set; }

        [XmlAttribute("version")]
        public string Version { get; set; }
    }
}
