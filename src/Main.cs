using System;

namespace Com.Aytacworld
{
    class Compiler
    {
        static void Main()
        {
            var config = ConfigGenerator.Create(@".\config.xml");
            var command = "";

            while (command != "exit")
            {
                command = Console.ReadLine();

                switch (command)
                {
                    case "build":
                        Console.WriteLine(config.Application.Title);
                        //config.Build();
                        break;
                    case "clean":
                        Console.WriteLine(config.Application.Description);
                        // config.Clean();
                        break;
                    case "run":
                        // config.Run();
                        break;
                    case "stop":
                        // config.Stop();
                        break;
                }
            }

            Console.WriteLine("Closing Compiler");
        }
    }
}
